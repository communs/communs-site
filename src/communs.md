# Les (biens) Communs

## Qu'est-ce qu'un Commun ?

Les biens communs, ou tout simplement communs, sont des **ressources**,
gérées collectivement par une **communauté**, celle-ci établit des
**règles** et une **gouvernance** dans le but de préserver et pérenniser
cette ressource.

En d'autres termes on peut définir les communs par une ressource (bien
commun) **plus** les interactions sociale au sein de la communauté
prenant soin de cette ressource.


![cdc2](images/cdc2.png)
Librement adapté de Les (biens) communs – Contours et repères, Villes en
Biens Communs
### Un modèle économique et social durable

Le concept de biens communs permet de libérer l’action économique d’une
approche tiraillée entre « plus d’état » et « plus de privé ». Centré
sur les ressources, c’est une économie qui se construit sur la gestion
collective et la préservation des ressources plutôt que sur la
compétition pour son exploitation. Par cela, les biens communs redonnent
du pouvoir et de l’autonomie aux citoyens, et prennent en compte
l’interdépendance de l’ensemble des parties prenantes et de la ressource
gérée. Il n’y a pas un modèle économique imposé aux communs, mais des
principes qui guident la gouvernance et la création de valeur issue de
l’usage d’une ressource.

### La renaissance des communs

Les biens communs ont une histoire de plusieurs siècles. Mais face à la
crise systémique, économique et environnementale à l’oeuvre depuis une
dizaine d’années, et le développement des cultures numériques libres,
cette théorie se réactualise et reprend une légitimité contemporaine. De
nombreux chercheurs et travaux étayent l’opportunité de la place des
communs dans un renouveau économique.

## Des exemples de communs


## Participer

Les communs existent déjà, et ce depuis longtemps. Ce que se propose le
mouvement des communs ce n'est pas de gérer ces communs.

L'un des objectif du mouvement des communs est de créer des assemblées
d'acteurs des communs, de mettre en lumière leur existence, et de les
préserver des *"enclosures"* et autres mode d'accaparement.

Si vous souhaitez participer à ce mouvement, inscrivez-vous sur la
[liste de discussion dédiée aux échanges autour des
communs](https://mail.cfcloud.fr/bienscommuns.org/subscribe/echanges "Liste Échanges").


## Aller plus loin

Quelques liens et livres si vous souhaitez creuser le sujet.

### Articles et sites dédiés

-   [Interviewn de David Bollier, *Les communs et nous*
    (Multinationales.org)](http://multinationales.org/David-Bollier-Les-communs-nous)
-   [Remix the Commons](http://www.remixthecommons.org/)

### Livres

-   Bollier, David, *La renaissance des communs : Pour une société de
    coopération et de partage*\
     (Paris: CHARLES LEOPOLD MAYER, 2014)
-   [Coriat, Benjamin, *Le retour des communs & la crise de l’idéologie
    propriétaire*](http://www.editionslesliensquiliberent.fr/livre-Le_retour_des_communs-9791020902726-1-1-0-1.html)\
     (Paris: LES LIENS QUI LIBERENT EDITIONS, 2015)
-   Vecam, *Libres savoirs : Les biens communs de la connaissance -
    Produire collectivement, partager et diffuser les connaissances au
    XXIe siècle*\
     (Caen: C&F Editions, 2011)

[Webibliographie complète sur le site de P2P
Foundation.](http://p2pfoundation.net/Bibliographie_Francophone)


## Mentions légales

Le site bienscommuns.org est hébergé en France, par ...

Ce site est édité par ...

